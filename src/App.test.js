import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import FizzBuzz from './fizzbuzz/FizzBuzz';

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<App />, div);
  ReactDOM.unmountComponentAtNode(div);
});

describe('test for fizzbuzz provider', () => {
	test('divisible by both 5 & 3', () => {
		expect(FizzBuzz(30)).toBe('fizz buzz');
	});
	test('divisible only by 5', () => {
		expect(FizzBuzz(10)).toBe('buzz');
	});
	test('divisible only by 3', () => {
		expect(FizzBuzz(21)).toBe('fizz');
	});
	test('divisible neither by 5 nor by 3', () => {
		expect(FizzBuzzProvider(34)).toBe(34);
	});
});
