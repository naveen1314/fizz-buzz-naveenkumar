import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import FizzBuzzComponent from './components/FizzBuzzComponent';

class App extends Component {
  render() {
    return (
      <div className="App">
        <header className="App-header">          
          <h1 className="App-title">Fizz Buzz</h1>
        </header>
        <br />
		<FizzBuzzComponent />
      </div>
    );
  }
}

export default App;
