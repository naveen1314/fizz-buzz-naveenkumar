const FizzBuzz = (value) => {
	var today = new Date().getDay();
	if(value % 15 === 0){
		return today === 3 ? 'wizz wuzz' : 'fizz buzz';
	}else if(value % 5 === 0){
		return today === 3 ? 'wuzz' : 'buzz';
	}else if(value % 3 === 0){
		return today === 3 ? 'wizz' : 'fizz';
	}else{
		return value;
	}
}

export default FizzBuzz;