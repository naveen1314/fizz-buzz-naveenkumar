import React, { Component } from 'react';
import { Button, FormGroup, FormControl, HelpBlock} from 'react-bootstrap';
import FizzBuzz from '../fizzbuzz/FizzBuzz';
import '../App.css';

class FizzBuzzComponent extends Component {
	constructor(props){
		super(props);
		this.handleChange = this.handleChange.bind(this);
		this.state = {
			currentInput : '',
			value: '',
			validationState: null,
			currentPage: 1,			
		}
	}	

  handleChange(e) {
    this.setState({ value: e.target.value });
	const inputValue = e.target.value;
		if (inputValue > 1000) this.setState({validationState : 'error'});
		else if (inputValue === '' || inputValue <= 0) this.setState({validationState : 'error'});
		else return this.setState({validationState : 'success'});
  }
  
  preaviousPage(){
	  if(this.state.currentPage >=2)
	  {
		this.setState({
		  currentPage : this.state.currentPage - 1		  
		});
	  }	  
  }
  
  nextPage(){
	  var lastPage = Math.ceil(this.state.value / 20);
	  
	  if(this.state.currentPage < lastPage)
	  {
		this.setState({
		  currentPage : this.state.currentPage + 1		  
		});
	  }	 	  
  }
  
  render() {
	const itemsPerPage = 20;
	var lastItemIndex = this.state.currentPage * itemsPerPage;
	var firstItemIndex = lastItemIndex - itemsPerPage;
	var data = this.state.validationState === 'success' ? [...Array(+this.state.value + 1).keys()]
	.slice(1)
	.slice(firstItemIndex,lastItemIndex) : [];
	var fizzBuzzList = this.state.validationState === 'success' ? data.map(x => {
		let result = FizzBuzz(x);
		var colorClass = '';
		result > 0 ? '' : colorClass = result;
		if(colorClass === 'fizz buzz'){
			return <li key={x}><span className='fizz'>fizz </span><span className='buzz'>buzz</span></li>;		
		}else if(colorClass === 'wizz wuzz'){
			return <li key={x}><span className='wizz'>wizz </span><span className='wuzz'>wuzz</span></li>;		
		}else{
			return <li key={x} className={colorClass}>{result}</li>;		
		}			
	}) : null;
    return (
      <div className="container">
		  <form>
			<FormGroup
			  controlId="formBasicText"
			  validationState={this.state.validationState}
			>			  
			  <FormControl
				type="number"
				value={this.state.value}
				placeholder="Enter a number"
				onChange={this.handleChange}
			  />
			  <FormControl.Feedback />
			  {this.state.validationState === 'error' && <HelpBlock>Please enter number in the range 1-1000</HelpBlock>}
			</FormGroup>
		  </form>		
		<br />
		<hr />
		<br />
		{this.state.validationState === 'success' && <div>
			<ul>
				{fizzBuzzList}
			</ul>
		</div>}
		<br />
		<div>
			<Button bsStyle="primary" className="btn" onClick={this.preaviousPage.bind(this)}>Previous</Button>
			<Button bsStyle="primary" className="btn" onClick={this.nextPage.bind(this)}>Next</Button>
		</div>
      </div>
    );
  }
}

export default FizzBuzzComponent;